<?php get_header(); ?>
	
<article id="page-home">
	<?php if (get_field('slides')): ?>
	<header id="home-header">
		<?php 
		$slides = get_field('slides');
		$random_slides=array_rand($slides,3);
		?>

		<ul class="owl-carousel">
			<?php foreach ($random_slides as $key => $value) { ?>
			<?php $slide = $slides[$value]; ?>
			<li>
				<figure class="home-header-bg hide-mobile" style="background-image: url(<?php echo $slide['background']['url'] ?>);"></figure>
				<figure class="home-header-bg show-mobile" style="background-image: url(<?php echo $slide['background_mobile']['url'] ?>);"></figure>
				<div class="content">
					<div class="container">
						<div class="home-header-text">
							<img src="<?php echo $slide['angle_box_mobile']['url'] ?>" alt="" class="show-mobile" width="100%" height="auto">
							<img src="<?php echo $slide['angle_box']['url'] ?>" alt="" class="hide-mobile" width="100%" height="auto">
							<div class="text">
								<h2><?php echo $slide['frase'] ?></h2>
								<h3>Seja um trainee EY e ajude a formular as perguntas que podem mudar tudo.</h3>
							</div>
						</div>
						
						<div class="home-header-footer">
							<div class="floatright">
								<h4><?php echo $slide['nome'] ?>  |  <?php echo $slide['descricao'] ?></h4>
								<h5>Be the future <br>#B<span>ey</span>ellow</h5>
							</div>
							<?php /*
							<div class="floatleft">
								<div class="bullets">
									<ul>
										<li></li>
										<li></li>
										<li></li>
									</ul>
								</div>
								<p>Quanto melhor a pergunta. Melhor a resposta. Melhor o mundo dos negócios.</p>
							</div>
							*/  ?>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</li>
			<?php } ?>
		</ul>

		<a href="#h-section-01" class="scroll-down"> <span>Scroll</span> </a>

	</header>
	<?php else: ?>
	<header id="home-header">
		<div class="home-header-video">
			<div class="container">
				<h2>Como atuar não apenas em uma, mas em várias empresas que você admira?</h2>
				<div class="home-header-footer">
					<div class="floatright">
						<h5>Be the future <br>#B<span>ey</span>ellow</h5>
					</div>
					<div class="floatleft">
						<div class="bullets">
							<ul>
								<li></li>
								<li></li>
								<li></li>
							</ul>
						</div>
						<p>Quanto melhor a pergunta. Melhor a resposta. Melhor o mundo dos negócios.</p>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<video src="https://beyellow.com.br/wp-content/uploads/2019/10/EY_CALL_MAIN_COMP-site_20191009.mp4" loop muted autoplay></video>
	</header>
	<?php endif; ?>

	<section id="call-to-action" class="show-mobile">
		<div class="container">
			<a href="http://www.vagas.com.br/v1642846" target="_blank" class="btn">Inscreva-se aqui</a>
		</div>
	</section>

	<section id="h-section-01">
		<?php /* $video = get_field('videohome_video'); if ($video): ?>
		<video poster="" id="bgvid" playsinline autoplay loop muted>
            <source src="<?php echo $video['url'] ?>" type="video/mp4">
        </video>
		<?php endif */ ?>
		<div class="content-cell">
			<div class="container">
				<h2><?php the_field('videohome_tagline') ?></h2>
				<h3><?php the_field('videohome_titulo') ?></h3>
				<?php the_field('videohome_texto') ?>
				<!-- <a href="<?php the_field('videohome_link') ?>" class="btn video-lightbox fancybox.iframe"><?php the_field('videohome_texto_btn') ?></a> -->
			</div>
		</div>
	</section>

	<section id="h-section-02">
		<div class="container">
			<h2 class="title-h2"><?php the_field('homeboxamarelo_titulo') ?></h2>
			<div class="table-content">
				<div class="table-col">
					<?php the_field('homeboxamarelo_texto') ?>
					<a href="<?php the_field('homeboxamarelo_link') ?>" class="btn"><?php the_field('homeboxamarelo_textobtn') ?></a>
				</div>
				<div class="table-col">
					<p><?php the_field('homeboxamarelo_texto_select') ?></p>
					<?php if (have_rows('homeboxamarelo_itens_select')): ?>
					<select name="areas" id="" class="select-cursos" style="width: 100%">
						<option value=""></option>
						<?php while(have_rows('homeboxamarelo_itens_select')): the_row(); ?>
						<option value="<?php the_sub_field('link') ?>"><?php the_sub_field('texto') ?></option>
						<?php endwhile; ?>
					</select>
					<?php endif ?>
				</div>
			</div>
			<a href="<?php the_field('homeboxamarelo_link') ?>" class="btn show-mobile"><?php the_field('homeboxamarelo_textobtn') ?></a>
		</div>
	</section>

	<section id="h-section-universum">
		<div class="table-container">
			<?php $image = get_field('boxbrancohome_imagem'); if ($image): ?>
			<div class="col-img" style="background-image: url(<?php echo $image['url']; ?>)"></div>
			<?php endif ?>
			<div class="col-text">
				<div class="content">
					<?php $image = get_field('boxbrancohome_logo'); if ($image): ?>
					<img src="<?php echo $image['url']; ?>" alt=""> 
					<?php endif ?>
					<h2><?php the_field('boxbrancohome_titulo') ?></h2>
					<?php the_field('boxbrancohome_texto') ?>
					<a href="<?php the_field('boxbrancohome_link') ?>" class="btn btn-primary"><?php the_field('boxbrancohome_textobtn') ?></a>
				</div>
			</div>
		</div>
	</section>

	<?php 
	if(have_rows('home_parallax')):
	$i=3; while(have_rows('home_parallax')): the_row();

	if (get_sub_field('layout')=="center"):
	?>
	<section id="h-section-<?php echo lead_zero($i) ?>">
		<?php $image = get_sub_field('imagem'); if ($image): ?>
		<figure class="bg-parallax" data-speed="15" style="background-image: url( <?php echo $image['url']; ?> )"></figure>
		<?php endif ?>
		<div class="home-panel2">
			<div class="home-panel2-body">
				<h2 class="title-h2"><?php the_sub_field('titulo') ?></h2>
				<?php the_sub_field('texto') ?>
				<p class="hashtag">#Beyellow</p>
				<a href="<?php the_sub_field('link') ?>" class="btn btn-primary"><?php the_sub_field('texto_btn') ?></a>
			</div>
		</div>
		<div class="bar-bottom">
			<div class="triangle"></div>
			<div class="bg"></div>
		</div>
	</section>
	<?php else: ?>
	<section id="h-section-<?php echo lead_zero($i) ?>" class="parallax-panel">
		<?php $image = get_sub_field('imagem'); if ($image): ?>
		<figure class="bg-parallax" data-speed="15" style="background-image: url( <?php echo $image['url']; ?> )"></figure>
		<?php endif ?>
		<div class="content-cell">
			<div class="container">
				<div class="home-panel <?php the_sub_field('layout') ?>">
					<div class="home-panel-header"></div>
					<div class="home-panel-body">
						<div class="content-cell">
							<h2 class="title-h2"><?php the_sub_field('titulo') ?></h2>
							<?php the_sub_field('texto') ?>
							<a href="<?php the_sub_field('link') ?>" class="btn"><?php the_sub_field('texto_btn') ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php endif; ?>

	<?php $i++; endwhile; endif; ?>

	<section id="h-section-06">

		<?php 
        $today = date('Ymd');

		$args = array(
            'post_type' =>'eventos',
            'posts_per_page' => 3,
            'meta_query' => array(
								array(
									'key' => 'data',
									'compare' => '>=',
									'value' => $today,
									)
								),
			'meta_key' => 'data',
			'orderby' => 'meta_value',
			'order' => 'ASC',
        );
		query_posts($args);
		if( have_posts() ):
		?>
		
		<div class="container">
			<h2 class="title-h2">Próximos eventos</h2>

			<div class="next-events">
				<ul class="owl-carousel">
					<?php 
					while (have_posts()): the_post();
						get_template_part('loop', 'eventos');
					endwhile; ?>
				</ul>
				<div class="allevents">
					<a href="<?php echo home_url() ?>/eventos" class="btn">Ver mais eventos</a>
				</div>
			</div>
		</div>
		<?php 
		endif;
		wp_reset_query();
		?>

	</section>


</article>


<?php get_footer(); ?>
