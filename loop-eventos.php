<?php setlocale(LC_ALL, 'pt_BR'); ?>
<li data-url="<?php the_permalink(); ?>">
	<div class="box-event">
		<a href="<?php the_permalink(); ?>">
			<figure>
				<?php 
				if (get_post_thumbnail_id()):
				$thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); 
				$thumb = aq_resize( $thumb_url[0], 300, 180, true, true, true );
				?>
				<img src="<?php echo $thumb ?>" alt="Thumbnail do evento <?php the_title(); ?>">
				<?php endif; ?>
			</figure>
		</a>
		<div class="box-event-info">
			<a href="<?php the_permalink(); ?>"><h4 class="event-date"><?php $data = get_field("data", false, false); $data = new DateTime($data); echo strftime('%d %b', $data->format('U')); ?></h4></a>
			<h3 class="event-title"><?php the_title(); ?></h3>
			<div class="full-description"><?php the_content(); ?></div>
			<span class='st_sharethis_custom' st_url="<?php the_permalink(); ?>" st_title="<?php the_title(); ?>" st_summary="<?php the_excerpt() ?>" st_image="<?php echo $thumb ?>" displayText='ShareThis'>Compartilhe</span>
		</div>
	</div>
</li>