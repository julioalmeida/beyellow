<?php 
/**
 * Template Name: Full Header
 *
 * Template com header full
 *
 */

get_header(); 

$post_slug=$post->post_name;

while (have_posts()) : the_post();

$image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); 
?>
<article id="page-<?php echo $post_slug ?>" class="page-content">
	<header class="header-full bg-parallax" data-speed="15" style="background-image:url('<?php echo $image[0] ?>');">
		<div class="content-cell">
			<h1 class="title-h1"><?php the_title() ?></h1>
			<?php if (get_field('subtitulo')): ?>
			<h2><?php the_field('subtitulo'); ?></h2>
			<?php endif ?>
		</div>
		<a href="#section1" class="scroll-down"> <span>Scroll</span> </a>
	</header>
	<?php get_template_part('parts/content', $post_slug); ?>
	<?php get_template_part("parts/template", "otherlinks"); ?>
</article>
<?php endwhile;?>
<?php get_footer(); ?>
