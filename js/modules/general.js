var generalModule = (function($) {

	function initializer(){

		initCommonEvents();

		initPageMenu();

		initCollapse();

		$(window).resize(function() {
			setTriangleSize();
		});

		$(document).ready(function() {
			setTriangleSize();
			scrollEvents();
		});

		$(window).scroll(function() {
			scrollEvents();
		});
	}


	function initCommonEvents(){
		$('.scroll-down').click(function() {

			var href = $(this).attr('href');
			var top = 0;

			if (href=="#section1") {
				top = $(this).closest('header').next('section').offset().top - $('#main-nav').outerHeight();
			}else{
				top = $(href).offset().top;
			}

			$('html, body').animate({scrollTop: top}, 500);

			return false;
		});

		$('.toggle-menu button').click(function() {
			$('body').toggleClass('open');
		});

		// $(".video-lightbox").fancybox({
		// 	width		: '100%',
		// 	height		: '100%',
		// 	padding		: 0,
		// 	fitToView	: true,
		// 	autoSize	: true,
		// 	closeClick	: false,
		// 	openEffect	: 'none',
		// 	closeEffect	: 'none'
		// });


		$(".select2").select2({
			minimumResultsForSearch: Infinity
		});

		var hash = window.location.hash;

		if (hash!=="") {
			if ($(hash).length) {
				var top = $(hash).offset().top - $('#main-nav').height();
				$(document).ready(function() {
					$('body, html').animate({
						scrollTop: top
						},
						300, function() {
						
						$(hash).find('.collapse-body').addClass('open');
						$(hash).find('.collapse-body').show();

					});
				});
			}
		}

		$('#events-list .box-event').click(function(event) {
			var obj = $(this);

			if (!$(event.target).hasClass('st_sharethis_custom')) {
				if (obj.hasClass('open')) {
					$('#events-list .box-event').removeClass('open');
				}else{
					$('#events-list .box-event').removeClass('open');
					obj.addClass('open');
				}
			}	

			return false;
		});

		var url = window.location;
		if ($('li[data-url="'+url+'"]').length) {
			var top = $('li[data-url="'+url+'"]').offset().top  - $('#main-nav').height() - 50;
			$('body, html').animate({
				scrollTop: top
				},
				300, function() {
				
				$('li[data-url="'+url+'"]').find('.box-event').addClass('open');

			});
		}
	}


	function initPageMenu(){
		$('#page-menu li a').click(function(event) {
			event.preventDefault();

			var $nav = $('#main-nav');
			var href = $(this).attr('href');
			var top = $(href).offset().top - $nav.height() - 50;
			if ($(window).width()<=1650) {
				top -= $('#page-menu').outerHeight();
			}

			if ($(window).width()<=740) {
				$('#page-menu ul').hide();
			}

			$('html,body').animate({scrollTop: top}, 500);
		});

		$('#page-menu span').click(function() {
			$(this).next('ul').toggle();
		});
	}


	function initCollapse(){
		$('.collapse-header').click(function() {

			if ($(this).closest('.collapse-item').hasClass('open')) {
				$(this).closest('.collapse-item').removeClass('open');
			}else{
				$(this).closest('.collapse-item').addClass('open');
			}

			$(this).next('.collapse-body').slideToggle();

			return false;
		});

		$('.collapse-body .close').click(function() {
			$(this).closest('.collapse-item').removeClass('open');
			$(this).closest('.collapse-body').slideToggle();

			return false;
		});
	}


	function scrollEvents(){
		var top = $(window).scrollTop();
		var navspace = 115;
		var $nav = $('#main-nav');
		var $pagemenu = $('#page-menu');

		if (top>10) {
			$('body').addClass('scroll');
		}else{
			$('body').removeClass('scroll');
		}

		if ($(window).width()>=768) {
			$('.bg-parallax').each(function(){
				var $obj = $(this);

				var rTop = top - $obj.offset().top;
			
				var yPos = -(rTop / $obj.data('speed')); 
		 
				var bgpos = '50% '+ yPos + 'px';
		 
				$obj.css('background-position', bgpos );
		 
			}); 
		}

		if ($('#page-menu').length) {
			var mtop = $('.header-default').outerHeight();
			
			if ($(window).width()>1650) {
				mtop += navspace;
			}
			
			var activePage = $pagemenu.find('li:first-child a').attr('href');

			if (top >= ($('.header-default').outerHeight() - $nav.outerHeight())) {
				if ($(window).width()>1650) {
					mtop = $nav.outerHeight() + navspace;
				}else{
					mtop = $nav.outerHeight();
				}
				$pagemenu.css('top', mtop);
				$pagemenu.addClass('fixed');
			}else{
				$pagemenu.css('top', mtop);
				$pagemenu.removeClass('fixed');
			}

			$pagemenu.css('opacity', '1');

			if (top <= $('.header-default').outerHeight()+navspace) {
				activePage = $pagemenu.find('li:first-child').children('a').attr('href');
				activePage = activePage.replace("#", "");
			}else{
				$('section').each(function(index, val) {
					if ( ($(val).offset().top - $nav.outerHeight() - $(window).outerHeight()*0.2) <= top ) {
						activePage = $(val).attr("id");
					}
				});
			}

			if ($pagemenu.find('a[href="#'+activePage+'"]').length) {
				$pagemenu.find('a').removeClass('active');
				$pagemenu.find('a[href="#'+activePage+'"]').addClass('active');
				var text = $pagemenu.find('a[href="#'+activePage+'"]').text();
				$pagemenu.find('span').text(text);
			}

			
		}
	}

	
	function setTriangleSize(){

		$('.home-panel').each(function(index, el) {
			var width = $(el).outerWidth();

			$(el).find('.home-panel-header').css('border-width', '0 0 70px '+width+'px');
		});

		$('.bar-bottom').each(function(index, el) {
			var width = $(el).outerWidth();
			var twidth = 330*width/1440;

			$(el).find('.triangle').css('border-width', '0 0 '+twidth+'px '+width+'px');
		});

		$('.image-with-bar .bar').each(function(index, el) {
			var width = $(el).outerWidth();
			var twidth = $(el).parent().height()-$(el).find('.bg').height()-30;

			$(el).find('.triangle').css('border-width', '0 0 '+twidth+'px '+width+'px');
		});

		var smoothScroll = new SmoothScroll('a[href*="#"]', {
      offset: 260
    });

	}


	initializer();

})(jQuery);