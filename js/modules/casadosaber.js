var casaModule = (function($) {

	function initializer(){
				
		var controller = new ScrollMagic.Controller();

		var headerHeight = $('header').height();
		var navHeight = $('#main-nav').height();
		
		// Fade Poligono
		new ScrollMagic.Scene({
			triggerElement: '.poligono-limit',
			triggerHook: 0,
			offset: '-99px',
			duration: '300px',
		})
		.setTween(TweenMax.to('.poligono-limit', 2, { autoAlpha: 0 }))
		.addTo(controller);

		// Fade Poligono
		new ScrollMagic.Scene({
			triggerElement: '.poligono-limit',
			triggerHook: 0,
			offset: '-99px',
			duration: '300px',
		})
		.setTween(TweenMax.to('.gsap-cell', 2, { css: { scale: 1.8 , marginBottom: '-80px' } }))		
		.addTo(controller);

		
		$(document).mousemove(function(event) {
			$('.mousemove').each(function(index, element) {
					let base = $(this).data('speed');
					let direction = $(this).data('direction') ? $(this).data('direction') : 1;
					let xPos = (event.clientX/$(window).width())-0.5;
					let yPos = (event.clientY/$(window).height())-0.5;
					let box = element;

					TweenLite.to(box, 1, {
							x: xPos * base * direction,
							y: yPos * base * direction,
							ease: Power1.easeOut,
					});
			});
		});


		$('.parallax-background').parallaxBackground({
			event: 'mouse_move',
			animation_type: 'rotate',
			animate_duration: 1,
			zoom: 70,
			rotate_perspective: 1000
		});

		$.each($('.gsap-fadeinup, .gsap-fadeinup-childs > *'), function (index, el) { 			

			new ScrollMagic.Scene({
				triggerElement: el,
				triggerHook: .95,
				// duration: "50%",
			})
			// .setClassToggle($(el), "visible") // add class to reveal
			.setTween(TweenMax.from(el, 2, { y: 50, autoAlpha: 0, ease: "power4.out", scale: 0.7 }))
			// .setTween(el, 0.5, {backgroundColor: "green", scale: 2.5})
			// .addIndicators({ name : 'title '}) // add indicators (requires plugin)
			.addTo(controller);
		});
		
		$.each($('.gsap-fadeinleft'), function (index, el) {
			new ScrollMagic.Scene({
				triggerElement: el,
				triggerHook: 1,
			})
			.setTween(TweenMax.from(el, 2, { x: -200, autoAlpha: 0, ease: "power4.out"}))
			.addTo(controller);
		});

		$.each($('.gsap-fadeinright'), function (index, el) {
			new ScrollMagic.Scene({
				triggerElement: el,
				triggerHook: .95,
			})
			.setTween(TweenMax.from(el, 2, { x: 200, autoAlpha: 0, ease: "power4.out"}))
			.addTo(controller);
		});

		

		$(document).on('submit', '#form-casadosaber', function(e) {
			e.preventDefault();

			toogleLoading(true);		

			var form = $('#form-casadosaber'),
					formData = getFormData(form)
					token = false;
						
			formData.service = 'casadosaber';
			formData.language = 'pt-br';
			formData.campaign = 'campanha-ey';
			
			registerUser(formData);

		});

		
		var	college = $('#college');

		$.ajax({
			url: cds.urlcolleges,
			success: function(data) {

				var selectData = data.map(function(item, index ){
					return { id: item.text , text : item.text }
				});
			
				college
				.select2({
					data: selectData,
					tags: true,
					createTag: function (params) {
						return {
							id: params.term,
							text: params.term,
							newOption: true
						}
					}
				})
				.on('select2:open', function() {
					
					$(this).data('select2').$dropdown.find(':input.select2-search__field').attr('placeholder', 'Digite aqui o nome da sua faculdade...')


					// however much room you determine you need to prevent jumping
					var requireHeight = 600;
					var viewportBottom = $(window).scrollTop() + $(window).height();
	
					// figure out if we need to make changes
					if (viewportBottom < requireHeight) 
					{           
							// determine how much padding we should add (via marginBottom)
							var marginBottom = requireHeight - viewportBottom;
	
							// adding padding so we can scroll down
							$(".aLwrElmntOrCntntWrppr").css("marginBottom", marginBottom + "px");
	
							// animate to just above the select2, now with plenty of room below
							$('html, body').animate({
									scrollTop: $("#mySelect2").offset().top - 10
							}, 1000);
					}
			});
			}
		});
			
	}

	function finishForm(error) {
		var form = $('#form-casadosaber');
		toogleLoading(false);
		if (error) {
			var msg = '<h3>Oops! Tivemos um problema com seu cadastro. Tente novamente em instantes :/</h3>';			
			form.addClass('finished --error');
		} else {
			var msg = '<h3>Seu cadastro foi realizado com sucesso! Veja sua caixa de e-mails e siga os próximos passos.</h3>';
			form.addClass('finished --success');
		}
		$('.form-message').html(msg);

	}

	function toogleLoading(status) {
		var form = $('#form-casadosaber');			
		if (status) {
			form.addClass('loading');
			form.removeClass('--msg');
		} else {
			form.removeClass('loading');
		}
	}

	function registerUser(data) {
		$.ajax({
			url: 'https://api.hiedu.co/v1/auth/registration/',
			type: 'post',
			dataType: 'json',
			contentType: 'application/json',
			success: function(res) {
				
				if (res.status == 'success') {
					registerUserContract(res.token, data.email)
				} else {
					finishForm(true);
				}

			},
			error: handleErrorMessage,
			data: JSON.stringify(data)
		})
	}

	function handleErrorMessage(err) {		
		var form = $('#form-casadosaber');
		form.addClass('--msg');
		form.find('.form-item-warning p').html(err.responseJSON.detail);
		toogleLoading(false);
	}

	function registerUserContract(token, email) {
		$.ajax({
			url: 'https://api.hiedu.co/v1/courses/casadosaber/contracts/',
			type: 'post',
			dataType: 'json',
			beforeSend: function (xhr) {
					xhr.setRequestHeader('Authorization', 'Bearer ' + token);
			},
			contentType: 'application/json',
			success: function(res) {
								
				finishForm(false);

			},
			error: handleErrorMessage,
			data: JSON.stringify({
				package: 'casadosaber-ey',
				service: 'casadosaber',
				email: email,
			})
		})
	}


	function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}


	function init(){
		if ($('body').hasClass('page-template-page-casadosaber')) {
			initializer();
		}
	}

	init();

})(jQuery);