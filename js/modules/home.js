var homeModule = (function($) {

	function initializer(){

		$('.next-events .owl-carousel').owlCarousel({
			responsive: {
				0: {
					items: 1.2,
					mouseDrag: true,
					touchDrag: true,
					pullDrag: true,
					loop: true,
					margin: 10
				},
				780: {
					items: 3,
					mouseDrag: false,
					touchDrag: false,
					pullDrag: false,
					loop: false,
					margin: 0
				}
			}
			
		});


		$('#home-header .owl-carousel').owlCarousel({
			items: 1,
			loop: true,
			nav: false,
			dots: false,
			autoplay: true,
			autoplayTimeout: 9000,
			autoplaySpeed: 500,
			mouseDrag: false,
			touchDrag: false,
			pullDrag: false,
			animateOut: 'fadeOut'
		});

		var selectCursos = $(".select-cursos").select2({
			placeholder: 'Cursos',
			minimumResultsForSearch: Infinity
		});

		selectCursos.on("select2:select", function (e) { 
			var value = e.currentTarget.value;
			window.location = value;
		});
	}


	if ($('body').hasClass('home')) {
		initializer();
	}

})(jQuery);