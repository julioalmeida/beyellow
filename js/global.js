require.config({
	"baseUrl": "/ey/wp-content/themes/yeopress/js",
	"paths": {
		"jquery": "vendor/jquery/dist/jquery",
		"select2": "vendor/select2/select2",
		"owlCarousel": "vendor/owl.carousel/dist/owl.carousel.min",
		"fancybox": "vendor/fancybox/jquery.fancybox",
		"general": "modules/general",
		"home": "modules/home"
	},
	'shim': {
        'owlCarousel': {
            deps: ['jquery']
        },
        'select2': {
            deps: ['jquery']
        },
        'fancybox': {
            deps: ['jquery']
        },
        'general': {
            deps: ['select2', 'fancybox']
        },
        'home': {
            deps: ['owlCarousel']
        }
    }
});

require(['jquery', 'select2', 'owlCarousel', 'fancybox', 'general', 'home'], function($) {

	generalModule.init();
	homeModule.init();

});
