<?php 
/**
 * Template Name: LP Casa do Saber
 *
 *
 */

get_header(); 

$post_slug=$post->post_name;

while (have_posts()) : the_post();

$image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); 
$bg_desktop = get_field('background_desktop');
$bg_mobile = get_field('background_mobile');

$path = get_template_directory_uri() . '/images/';
$mobile_maos = $path . 'lp-casadosaber-mobile-maos.png';
?>

<article id="page-<?php echo $post_slug ?>" class="page-content">
	<header class="">
		<div class="content">

			<div class="poligono-limit mousemove" data-speed="30" data-direction="-1">
				<div class="poligono-wrapper">
					<div class="poligono-content">
						<h1><?php the_title() ?></h1>
						<p>6 conteúdos gratuitos para quem acredita que o futuro do trabalho começa agora.</p>
					</div>
				</div>
			</div>

			<div class="bgs bgs--cell mousemove d-none d-md-block" data-speed="15" data-direction="1"></div>
			<img src="<?php echo $mobile_maos ?>" class="gsap-cell d-md-none" />
		</div>
	</header>
			<!-- <img src="<?php echo $bg_mobile; ?>" alt="<?php the_title() ?>" class="d-sm-none" />
			<img src="<?php echo $bg_desktop; ?>" alt="<?php the_title() ?>" class="d-none d-sm-block" /> -->
	
	<section id="intro" class="bg-yellow">
		<div class="container">
			<div class="text gsap-fadeinleft">
				<?php the_field('conteudo'); ?>
			</div>
			<div class="banner  gsap-fadeinright">
				<?php 
					$image = get_field('banner');
					$link = get_field('link_banner');
					if( !empty( $image ) ): ?>
						<?php if ($link) :?>
								<a data-fancybox class="" href="<?php echo $link['url'] ?>" title="<?php echo $link['title'] ?>" target="<?php echo $link['target'] ?>">
						<?php endif; ?>
							<img src="<?php echo esc_url($image['url']); ?>" class="img-fluid" alt="<?php echo esc_attr($image['alt']); ?>" />
						<?php if ($link) :?>
								</a>
						<?php endif; ?>
				<?php endif; ?>	
				<!-- <div class="d-none d-desktop-block">
					<div class="parallax-background" data-fancybox href="<?php echo $link['url'] ?>" title="<?php echo $link['title'] ?>">
						<div class="parallax-inner" style="background-image: url('<?php echo esc_url($image['url']); ?>')">
						</div>
					</div>
				</div> -->
			</div>
		</div>
	</section>

	<section id="categorias" class="bg-dark">
		<div class="container">
			<h2 class="gsap-fadeinup"><?php the_field('texto_categorias'); ?></h2>
			<?php 
				$icones = get_field('icones'); 
				if ($icones) {
					echo '<div class="icones">';
						foreach ($icones as $icone) {							
							echo '<div class="icone gsap-fadeinup"><div class="icone-img" style="background-image: url('.$icone['url'].')" ></div><div class="icone-title"><p>'.$icone['title'].'</p></div></div>';
						}
					echo '</div>';
				}
			?>
		</div>
	</section>

	<section id="detalhes">
		<div class="container">
			<div class="left">
				<?php the_field('titulo_secao'); ?>
			</div>
			<div class="right">
				<?php the_field('detalhes'); ?>
			</div>
		</div>

		<div class="container">
			<form id="form-casadosaber">
				<div class="form-item-row form-item-warning">
					<p class="message"></p>
				</div>
				<div class="form-item">
					<label for="nome">Nome completo</label>
					<input type="text" required value="" name="name" >
				</div>
				
				<div class="form-item">
					<label for="email">E-mail</label>
					<input type="email" required value="" name="email" >
				</div>
				
				<div class="form-item">
					<label for="faculdade">Faculdade</label>
					<select id="college" type="text" required value="" name="college" >
						<option></option>
					</select>
				</div>
				
				<div class="form-item">
					<label for="curso">Curso</label>
					<input type="text" required value="" name="college_course" >
				</div>

				<div class="form-item-row form-item form-item-submit">
					<input class="btn btn-yellow" type="submit" value="Enviar">
					<div class="loader">
						<div class="lds-facebook"><div></div><div></div><div></div></div>
					</div>
				</div>

			</form>
			<div class="form-message"></div>
		</div> <!-- container form-casadosaber -->

		<div class="container detalhesrodape">
				<div class="stores">
					<?php 
						if (get_field('link_google_play')) {
							echo '<a title="Clique para fazer o download do app" target="_blank" rel="nofollow" href="'.get_field('link_google_play').'">',
											'<img src="'.get_template_directory_uri().'/images/download-google-play.png" />',
										'</a>';
						}
						
						if (get_field('link_apple_store')) {
							echo '<a title="Clique para fazer o download do app" target="_blank" rel="nofollow" href="'.get_field('link_apple_store').'">',
											'<img src="'.get_template_directory_uri().'/images/download-app-store.png" />',
										'</a>';
						}
					?>
				</div>
				<div class="texto-extra">
					<?php the_field('texto_rodape'); ?>
				</div>
		</div>
	</section>

	<?php // get_template_part('parts/content', $post_slug); ?>
	<?php get_template_part("parts/template", "otherlinks"); ?>
</article>
<?php endwhile;?>
<?php get_footer(); ?>
