<?php $id = $post->ID; if (is_archive()){ $id = 19; } ?>
<?php if (get_field('paginas', $id)): $paginas = get_field('paginas', $id); ?>
<section id="other-links">
	<div class="container">
		<hr>
		<h2>Veja também</h2>
		<ul>
			<?php foreach ($paginas as $key => $value) { ?>
			<li>
				<a href="<?php the_permalink($value->ID) ?>">
					<?php if (get_field('imagem', $value->ID)): ?>
					<figure>
						<?php 
						$thumb_url = get_field('imagem', $value->ID);
						$thumb = aq_resize( $thumb_url['url'], 450, 250, true, true, false );
						?>
						<img src="<?php echo $thumb ?>" alt="Thumbnail da páginas <?php echo $value->post_title; ?>">
					</figure>
					<?php endif ?>
					<h3><?php echo $value->post_title; ?></h3>
				</a>
			</li>
			<?php } ?>
		</ul>
	</div>
</section>
<?php endif ?>