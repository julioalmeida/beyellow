<?php if( have_rows('exp_boxes') ): ?>	
<section id="experience-boxes">

	<h2><?php the_field('boxes_titulo') ?></h2>

	<?php $i=1; while ( have_rows('exp_boxes') ) : the_row(); ?>
	
		<?php $img = get_sub_field('imagem');?>

		<?php if (($i%2)>0): ?>
		<div class="box-image box-yellow">
			<div class="box-text">
				<h3 class="title-h3"><?php the_sub_field('titulo') ?></h3>
				<p><?php the_sub_field('texto') ?></p>
			</div>
			<?php if ($img): ?>
			<div class="box-img" style="background-image: url(<?php echo $img['url'] ?>)"></div>
			<?php endif ?>
		</div>
		<?php else: ?>
		<div class="box-image">
			<?php if ($img): ?>
			<div class="box-img" style="background-image: url(<?php echo $img['url'] ?>)"></div>
			<?php endif ?>
			<div class="box-text">
				<h3 class="title-h3"><?php the_sub_field('titulo') ?></h3>
				<p><?php the_sub_field('texto') ?></p>
			</div>
		</div>
		<?php endif; ?>
	
	<?php $i++; endwhile; ?>

</section>
<?php endif; ?>


<?php if( have_rows('motivos_itens') ): ?>	
<section id="experience-motivos">
	
	<div class="collapse">
		<div class="container">
			<h2><?php the_field('motivos_titulo') ?></h2>

			<?php $i=1; while ( have_rows('motivos_itens') ) : the_row(); ?>
			<div class="collapse-item item-number">
				<div class="collapse-header">
					<a href="#">
						<span class="number"><?php echo $i ?></span>
						<h3 class="collapse-title"><?php the_sub_field('titulo') ?></h3>
						<button class="collapse-button big secondary"><span>Open</span></button>
					</a>
				</div>
				<div class="collapse-body">
					<div class="table-content">
						<div class="col-text">
							<?php the_sub_field('texto') ?>
						</div>
					</div>
					<button class="collapse-button big close secondary"><span>Close</span></button>
				</div>
			</div>
			<?php $i++; endwhile; ?>

		</div>
	</div>

</section>
<?php endif; ?>

<?php if( have_rows('exp_secoes') ): ?>	
<?php $i=1; while ( have_rows('exp_secoes') ) : the_row(); ?>
<section id="experience-text-<?php echo $i ?>">
	<div class="text-content">
		<div class="container">
			<h2 class="title-h2 page-subtitle"><?php the_sub_field('titulo') ?></h2>
			<?php the_sub_field('texto') ?>
		</div>
	</div>
</section>
<?php $i++; endwhile; ?>
<?php endif; ?>