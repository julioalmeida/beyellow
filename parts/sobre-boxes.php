<section id="sobre-boxes">
	

	<?php if (have_rows('sobre_sub_boxes')): ?>
	<?php $i=1; while (have_rows('sobre_sub_boxes')): the_row() ?>
	<div class="box-image">
		<?php 
		$image = get_sub_field('imagem');

		if (($i%2)>0 && $image): ?>
			<div class="box-img" style="background-image: url(<?php echo $image['url'] ?>)"></div>
		<?php endif ?>
		<div class="box-text">
			<h3 class="title-h3"><?php the_sub_field('titulo') ?></h3>
			<?php the_sub_field('texto') ?>
		</div>
		<?php if (($i%2)==0 && $image): ?>
			<div class="box-img" style="background-image: url(<?php echo $image['url'] ?>)"></div>
		<?php endif; ?>
		
	</div>
	<?php $i++; endwhile; ?>
	<?php endif ?>


	<?php if (have_rows('sobre_sub_colunas')): ?>
	<div class="programs-list">
		<div class="container">
			<ul>
				<?php while (have_rows('sobre_sub_colunas')): the_row() ?>
				<li>
					<?php $image = get_sub_field('imagem'); if($image): ?>
					<figure>
						<img src="<?php echo $image['url'] ?>" alt="">
					</figure>
					<?php endif ?>
					<?php the_sub_field('texto') ?>
				</li>
				<?php endwhile; ?>
			</ul>
			<div class="clearfix"></div>
		</div>
	</div>
	<?php endif ?>

	
	<?php if (have_rows('sobre_sub_boxes_branco')): ?>
	<?php $i=1; while (have_rows('sobre_sub_boxes_branco')): the_row() ?>
	<div class="box-image white">
		<?php 
		$image = get_sub_field('imagem');

		if (($i%2)>0 && $image): ?>
			<div class="box-img" style="background-image: url(<?php echo $image['url'] ?>)"></div>
		<?php endif ?>
		<div class="box-text">
			<?php the_sub_field('texto') ?>
		</div>
		<?php 
		
		if (($i%2)==0 && $image): ?>
		?>
			<div class="box-img" style="background-image: url(<?php echo $image['url'] ?>)"></div>
		<?php endif; ?>
		
	</div>
	<?php $i++; endwhile; ?>
	<?php endif ?>
	
</section>