<section id="sobre-<?php echo $post->post_name ?>">
	<div class="text-content">
		<div class="container">
			<h2 class="title-h2 page-subtitle"><?php the_title() ?></h2>
			<?php the_content(); ?>
		</div>
	</div>
	
	<?php if (get_field('presenca_frase')): ?>
	<div class="presenca-map" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/mapa-mundi.svg)">
		<div class="content-cell">
			<h4><?php the_field('presenca_frase') ?></h4>
		</div>
	</div>
	<?php endif ?>
	
	<?php if (get_field('presenca_texto_2')): ?>
	<div class="text-content">
		<div class="container">
			<?php the_field('presenca_texto_2') ?>
		</div>
	</div>
	<?php endif ?>
	
	<?php if (get_field('presenca_mapa_numeros') || get_field('presenca_mapa_cidades')): ?>
	<div class="brasil-map">
		<?php 
		$image = get_field('presenca_mapa_imagem');

		if ($image):
		?>
		<figure class="map">
			<img src="<?php echo $image['url'] ?>" alt="">
		</figure>
		<?php endif; ?>
		
		<div class="brasil-text">

			<?php if (have_rows('presenca_mapa_numeros')): ?>
			<ul class="numbers">
				<?php while(have_rows('presenca_mapa_numeros')): the_row() ?>
					<li><?php the_sub_field('texto') ?></li>
				<?php endwhile; ?>
			</ul>
			<?php endif ?>

			<?php if (have_rows('presenca_mapa_cidades')): ?>
			<ul class="cities">
				<?php while(have_rows('presenca_mapa_cidades')): the_row() ?>
					<li><?php the_sub_field('texto') ?></li>
				<?php endwhile; ?>
			</ul>
			<?php endif ?>
			
		</div>
		<div class="clearfix"></div>
	</div>
	<?php endif ?>

	<div class="clearfix"></div>
</section>