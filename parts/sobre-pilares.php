<section id="sobre-<?php echo $post->post_name ?>">
	<div class="text-content">
		<div class="container">
			<h2 class="title-h2 page-subtitle"><?php the_title() ?></h2>
			<?php the_content(); ?>
		</div>
	</div>


	<?php 
	$image = get_field('pilares_img');

	if ($image):
	?>
	<div class="image-with-bar">
		<img src="<?php echo $image['url'] ?>" alt="">
		<div class="bar">
			<div class="triangle"></div>
			<div class="bg"></div>
		</div>
	</div>
	<?php endif; ?>

	<?php if (get_field('pilares_texto_amarelo')): ?>
	<div class="bg-yellow">
		<div class="text-content">
			<div class="container">
				<?php the_field('pilares_texto_amarelo') ?>
			</div>
		</div>
	</div>
	<?php endif ?>
	
	<div class="clearfix"></div>
</section>