<?php
$childpages = get_pages( array( 'child_of' => $post->ID, 'sort_column' => 'menu_order' ) );
?>
<nav id="page-menu">
	<span><?php the_field('titulo_submenu', $childpages[0]->ID); ?></span>
	<ul>
		<?php foreach ($childpages as $key => $value) { ?>
		<?php if (get_field('titulo_submenu', $value->ID)): ?>
		<li><a href="#sobre-<?php echo $value->post_name ?>"><?php the_field('titulo_submenu', $value->ID); ?></a></li>
		<?php endif ?>
		<?php } ?>
	</ul>
</nav>

<?php 
$args = array(
            'post_type' =>'page',
            'post_parent' => $post->ID,
            'posts_per_page' => -1,
            'orderby'           => 'menu_order',
            'order'           => 'ASC'
        );
query_posts($args);
if( have_posts() ):
while (have_posts()): the_post();
	get_template_part("parts/sobre", $post->post_name);
endwhile;
endif;
wp_reset_query();
?>

<?php /* 
<section id="sobre-entrevistas">
	<div class="box-image video">
		<div class="box-text">
			<p>O <strong>EY Able</strong>, por sua vez, é um programa voltado para pessoas com deficiência, que fortalece o compromisso da EY com a diversidade e a inclusão, proporcionando aprendizado que ajuda essas pessoas a atingir seu potencial e contribuindo com o objetivo maior da EY que é construir um mundo dos negócios melhor. <a href="#">Veja algumas entrevistas.</a></p>
		</div>
		<div class="box-img">
			<img src="<?php echo get_template_directory_uri(); ?>/images/img-box4.jpg" alt="">
		</div>
	</div>
</section>
*/ ?>