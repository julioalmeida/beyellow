<section id="events-list">
	<div class="container">
		<?php 
        $today = date('Ymd');
		$args = array(
            'post_type' =>'eventos',
            'posts_per_page' => -1,
            'meta_query' => array(
								array(
									'key' => 'data',
									'compare' => '>=',
									'value' => $today,
									)
								),
			'meta_key' => 'data',
			'orderby' => 'meta_value',
			'order' => 'ASC',
        );
		query_posts($args);
		if( have_posts() ):
		?>
		<h3 class="title-h3">Veja os próximos eventos da EY </h3>
		<ul>
			<?php 
			while (have_posts()): the_post();
				get_template_part('loop', 'eventos');
			endwhile; ?>
		</ul>
		<?php else: ?>
		<h3 class="title-h3">Não há eventos cadastrados</h3>
		<?php
		endif;
		wp_reset_query();
		?>

	</div>
</section>