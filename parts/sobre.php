<section id="sobre-<?php echo $post->post_name ?>">
	<div class="text-content">
		<div class="container">
			<h2 class="title-h2 page-subtitle"><?php the_title() ?></h2>
			<?php the_content(); ?>
		</div>
	</div>
	<div class="clearfix"></div>
</section>