<?php $secoes = get_field('programa_secoes'); ?>

<section id="programa-intro">
	<div class="content-cell">
		<div class="container">
			<h2><?php echo $secoes[0]['titulo'] ?></h2>
			<?php echo $secoes[0]['texto'] ?>
		</div>
	</div>
</section>

<section id="programa-etapas">
	<div class="text-content">
		<div class="container">
			<h2 class="title-h2 page-subtitle"><?php echo $secoes[1]['titulo'] ?></h2>
			<?php echo $secoes[1]['texto'] ?>
		</div>
	</div>
</section>

<section id="programa-calendario">
	<div class="container">
		<div class="table-content">
			<div class="col-icon">
				<img src="https://beyellow.com.br/wp-content/uploads/2017/01/icon-calendar.svg" alt="" width="80" height="auto">
			</div>
			<div class="col-text">
				<h3 class="title-h3"><?php echo $secoes[2]['titulo'] ?></h3>
				<?php echo $secoes[2]['texto'] ?>
				<a href="http://www.vagas.com.br/v1642846" target="_blank" class="btn">Inscreva-se</a>
			</div>
		</div>
	</div>
</section>


<?php if( have_rows('cursos') ): ?>	
<section id="programa-etapas">
	<div class="text-content">
		<div class="container">
			<h2 class="title-h2 page-subtitle"><?php the_field('areas_titulo') ?></h2>
			<?php the_field('areas_texto') ?>
		</div>
	</div>

	<div class="collapse">
		<div class="container container-900">
			<p><strong><?php the_field('areas_subtitulo') ?></strong></p>
		</div>
		<div class="container">
			
			<?php while ( have_rows('cursos') ) : the_row(); ?>
			<div class="collapse-item" id="curso-<?php echo strtolower(preg_replace("/[^A-Za-z0-9]/", "", get_sub_field('titulo'))) ?>">
				<div class="collapse-header collapse-header-big">
					<a href="#">
						<h3 class="collapse-title"><?php the_sub_field('titulo') ?></h3>
						<button class="collapse-button big"><span>Open</span></button>
					</a>
				</div>
				<div class="collapse-body">
					<div class="table-content">
						<div class="col-text"><?php the_sub_field('texto') ?></div>
					</div>
					<button class="collapse-button close big secondary"><span>Close</span></button>
				</div>
			</div>
			<?php endwhile; ?>
			
		</div>
	</div>
</section>
<?php endif; ?>


<?php if( have_rows('faq') ): ?>	
<section id="programa-faq">
	<div class="text-content">
		<div class="container">
			<h2 class="title-h2 page-subtitle"><?php the_field('faq_titulo') ?></h2>
		</div>
	</div>
	<div class="collapse">
		<div class="container">
			<?php while ( have_rows('faq') ) : the_row(); ?>
			<div class="collapse-item">
				<div class="collapse-header">
					<a href="#">
						<h3 class="collapse-title"><?php the_sub_field('pergunta') ?></h3>
						<button class="collapse-button"><span>Open</span></button>
					</a>
				</div>
				<div class="collapse-body">
					<?php the_sub_field('resposta') ?>
					<button class="collapse-button close secondary"><span>Close</span></button>
				</div>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>