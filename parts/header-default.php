<nav id="main-nav">
    <div class="container">
        <div class="nav-left">
            <div class="logo">
                <a href="<?php echo home_url() ?>">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/logo-ey-icon.svg" alt="Logo EY Icon" class="icon" width="82" height="auto">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/logo-ey-text.svg" alt="Logo EY" class="full" width="181" height="auto">
                </a>
            </div>
        </div>
        <div class="nav-right">
            <div class="btn-action">
                <a href="http://www.vagas.com.br/v1815388" target="_blank" class="btn btn-primary">Inscreva-se</a>
            </div>
            <div class="toggle-menu">
                <button>
                    <span class="label">Menu</span>
                    <span class="toggle"></span>
                </button>
            </div>
        </div>
    </div>
</nav>

<div id="full-menu">
    
    <div class="container">
        <div class="toggle-menu">
            <button>
                <span class="label">Menu</span>
                <span class="toggle"></span>
            </button>
        </div>
    </div>

    <div id="main-menu">
        <div class="content-cell">
            <?php 
            wp_nav_menu(array(
                'menu' => 'main-nav',
                'container' => false,
                'menu_class' => 'primary-menu'
            ));
            ?>
            <?php social_links(); ?>
            <div class="call-to-action show-mobile">
                <a href="http://www.vagas.com.br/v1815388" target="_blank" class="btn">Inscreva-se</a>
            </div>
        </div>
    </div>

</div>