<section id="sobre-<?php echo $post->post_name ?>">
	<div class="text-content">
		<div class="container">
			<h2 class="title-h2 page-subtitle"><?php the_title() ?></h2>
			<?php the_content(); ?>
		</div>
	</div>
	<?php if (have_rows('atuacao_itens')): ?>
	<div class="collapse">
		<div class="container">
			<?php while (have_rows('atuacao_itens')): the_row() ?>
			<div class="collapse-item">
				<div class="collapse-header collapse-header-big">
					<a href="#">
						<h3 class="collapse-title"><?php the_sub_field('titulo') ?></h3>
						<button class="collapse-button big"><span>Open</span></button>
					</a>
				</div>
				<div class="collapse-body">
					<div class="table-content">
						<div class="col-text">
							<?php the_sub_field('texto') ?>
						</div>
						<?php 
						$image = get_sub_field('imagem');

						if ($image):
						?>
						<div class="col-image">
							<img src="<?php echo $image['url'] ?>" alt="">
						</div>
						<?php endif; ?>
					</div>
					<button class="collapse-button big close secondary"><span>Close</span></button>
				</div>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
	<?php endif ?>

	<?php if (get_field('atuacao_texto_2')): ?>
	<div class="text-content">
		<div class="container">
			<?php the_field('atuacao_texto_2') ?>
		</div>
	</div>
	<?php endif ?>
	<div class="clearfix"></div>
</section>