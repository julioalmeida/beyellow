<?php if (have_rows('timeline_etapas')): ?>
<section id="plano-intro">
	<div class="container">
		<h2><?php the_field('timeline_texto_intro') ?></h2>
		<h3><?php the_field('timeline_titulo') ?></h3>
		<a href="#" class="scroll-down"> <span>Scroll</span> </a>
	</div>
</section>


<section id="plano-timeline">
	
	<div class="container">
		<?php $i=1; while (have_rows('timeline_etapas')): the_row() ?>
		<div class="timeline-item" id="timeline-item-0<?php echo $i ?>">
			<h2 class="title-h2 page-subtitle"><?php the_sub_field('titulo') ?></h2>
			<?php
			$text = get_sub_field('texto');
			$itens = explode("\n", $text);
			?>
			<p>
				<?php foreach ($itens as $key => $value) { ?>
					<?php if ($value!==""): ?>
						<span class="arial">●</span> <?php echo $value ?>
					<?php endif ?>
				<?php } ?>
			</p>
			<div class="div"></div>
		</div>
		<div class="clearfix"></div>
		<?php $i++; endwhile; ?>
		<div class="call-to-action">
			<a href="http://www.vagas.com.br/v1642846" target="_blank" class="btn btn-primary btn-lg">Venha ser ey. Inscreva-se aqui</a>
		</div>
	</div>

</section>
<?php endif ?>


<section id="sobre-proposito">
	<div class="text-content">
		<div class="container">
			<h2 class="title-h2 page-subtitle"><?php the_field('carreira_des_titulo') ?></h2>
			<?php the_field('carreira_des_texto') ?>
		</div>
	</div>
	<?php if (have_rows('carreira_des_itens')): ?>
	<div class="collapse">
		<div class="container">
			<?php while (have_rows('carreira_des_itens')): the_row() ?>
			<div class="collapse-item">
				<div class="collapse-header collapse-header-big">
					<a href="#collapse-item-01">
						<h3 class="collapse-title"><?php the_sub_field('titulo') ?></h3>
						<button class="collapse-button big"><span>Open</span></button>
					</a>
				</div>
				<div class="collapse-body">
					<div class="table-content">
						<div class="col-text"><?php the_sub_field('texto') ?></div>
					</div>
					<button class="collapse-button big close secondary"><span>Close</span></button>
				</div>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
	<?php endif ?>
</section>