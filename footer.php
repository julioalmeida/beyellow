		<div class="clearfix"></div>

			<footer id="main-footer">
				<?php /*
				<div class="footer-links">
					<div class="table-content">
						<div class="col"> <img src="<?php echo get_template_directory_uri(); ?>/images/logo_footer-reconhecimento.png" alt=""> </div>
						<div class="col"> <img src="<?php echo get_template_directory_uri(); ?>/images/logo_footer-universum.png" alt="" > </div>
						<div class="col"> <img src="<?php echo get_template_directory_uri(); ?>/images/logo_footer-cubic.png" alt=""> </div>
						<div class="col"> <img src="<?php echo get_template_directory_uri(); ?>/images/logo_footer-exame.png" alt=""> </div>
						<div class="col"> <img src="<?php echo get_template_directory_uri(); ?>/images/logo_footer-vocesa.png" alt=""> </div>
						<div class="col"> <img src="<?php echo get_template_directory_uri(); ?>/images/logo_footer-weps.png" alt=""> </div>
						<div class="col"> <img src="<?php echo get_template_directory_uri(); ?>/images/logo_footer-proetica.png" alt=""> </div>
					</div>
				</div> 
				*/ ?>
				<div class="footer-social">
					<div class="footer-social-table">
						<div class="footer-social-networks">
							<p>Siga a EY nas redes sociais</p>
							<?php social_links(); ?>
						</div>
						<div class="footer-social-share">
							<p>Compartilhe</p>
							<a href="whatsapp://send?text=http://beyellow.com.br" data-action="share/whatsapp/share" class="share-whatsapp">Compartilhe este site via whatsapp</a>
						</div>
					</div>
					<p class="copyright">
						Todos os direitos reservados © EY <?php echo date('Y') ?>
					</p>
				</div>
			</footer>
			

		</main>
		
		<?php wp_footer() ?>
	</body>
</html>
